// ==UserScript==
// @name           RED: checkboxes to catch-up multiple collages without reloading for each
// @description    https://redacted.ch/forums.php?action=viewthread&threadid=1744&postid=600944#post600944
// @author         _
// @version        0.4
// @match          https://redacted.ch/userhistory.php?action=subscribed_collages
// @run-at         document-end
// @namespace      _
// ==/UserScript==

(() => {
  'use strict';

  const oldCatchUpLink = document.querySelector('.linkbox a[href*="catchup_collages"]');
  const auth = oldCatchUpLink.href.match(/auth=[a-z0-9]+/i);
  const collages = [...document.querySelectorAll('.subscribed_collages_table a[href^="collage.php"]')];

  if (collages.length <= 1) return;

  const createCheckbox = value => {
    const checkbox = document.createElement('input');
    checkbox.type= 'checkbox';
    checkbox.className = 'collage_box';
    checkbox.name = 'checked_collages[]';
    checkbox.value = value;
    checkbox.style.marginRight = '6px';
    return checkbox;
  }

  collages.forEach(collage => {
    const collageId = collage.href.split("=")[1];
    const checkbox = createCheckbox(collageId);
    collage.insertAdjacentElement('beforebegin', checkbox);
  });

  const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

  const catchUpChecked = async () => {
    const checkedBoxes = [...document.querySelectorAll('input[name="checked_collages[]"]:checked')];

    for (let checkbox of checkedBoxes) {
      try {
        const { value: collageId } = checkbox;
        await fetch(`/userhistory.php?action=catchup_collages&${auth}&collageid=${collageId}`);

        const parentTable = checkbox.closest('table');
        parentTable.style.display = 'none';
        parentTable.nextElementSibling.style.display = 'none';

        await wait(25);
      } catch (e) {
        console.error(`Failed to catch up (#${checkbox.value}):`, e);
      }
    }
  };

  const catchUpLink = document.createElement('a');
  catchUpLink.href = 'javascript:;';
  catchUpLink.classList.add('brackets');
  catchUpLink.addEventListener('click', catchUpChecked);
  catchUpLink.innerText = 'Catch Up Checked';

  oldCatchUpLink.insertAdjacentElement('afterend', catchUpLink);
  oldCatchUpLink.innerText = 'Catch Up All';
})();
